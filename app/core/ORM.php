<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 12/25/18
 * Time: 3:22 PM
 */

class ORM {

    private $sql = '';
    private $select = "SELECT";
    private $fields = "*";
    private $distinct = "DISTINCT";
    private $from = "FROM";
    private $order = "ORDER BY";
    private $limit = "LIMIT";
    private $limitLimit  = '';
    private $db = '';
    private $where = "WHERE";
    private $cond = [];
    private $rel = [];
    private $whereCols = [];
    private $whereVals = [];
    private $insert = "INSERT INTO";
    private $values_start = "VALUES (";
    private $values_end = ")";
    private $whereCalled = false;

    private $host = "localhost";
    private $dbname = "phpzii";
    private $dbuser = "root";
    private $dbpass = "12345";

    public function __construct() {

        $dsn = "mysql:host=" . $this->host . ";dbname=" . $this->dbname;

        try {
            $this->db = new PDO($dsn, $this->dbuser, $this->dbpass);
        }catch (PDOException $ex) {
            die('Database connection error!');
        }

    }

    ############### SELECT STATEMENT START ##################
    public function select($tbl) {

        $this->sql = implode(" ", array($this->select,$this->fields,$this->from,$tbl));
        return $this;
    }

    public function distinct($col, $tbl) {

        $this->sql = implode(" ", array($this->select,$this->distinct,$col,$this->from,$tbl));
        return $this;
    }

    public function isNull($col) {

        $this->sql = implode(" ", array($this->sql,$this->where,$col,"IS NULL"));
        return $this;
    }

    public function isNotNull($col) {

        $this->sql = implode(" ", array($this->sql,$this->where,$col," IS NOT NULL"));
        return $this;
    }


    public function operator(string ...$rel) {

        $this->rel[] = $rel;
        return $this;
    }


    public function where(string ...$where) {

        $this->cond[] = $where;

        return $this;
    }

    private function whereCols() {

        for ($i=0; $i<count($this->cond); $i++) {

            if(!empty($this->cond[0][2])) {
                $this->whereCols[] = $this->cond[$i][0] . " ". $this->cond[$i][1] ." ? " . $this->rel[$i][0];
                $this->whereVals[] = $this->cond[$i][2];
            } else {
                $this->whereCols[] = $this->cond[$i][0] . " = ? " . $this->rel[$i][0];
                $this->whereVals[] = $this->cond[$i][1];
            }

        }
    }


    private function whereExec() {

        if (!$this->whereCalled) {

            if (count($this->cond)) {
                $this->whereCols();
                $this->sql = implode(" ", array($this->sql,$this->where,implode(" ", $this->whereCols)));
                $this->whereCalled = true;
            }

        }

    }

    public function orderBy($col, $mode) {

        $this->whereExec();
        $this->sql = implode(" ", array($this->sql,$this->order,$col,$mode));
        return $this;
    }

    public function limit($start, $records) {

        $this->whereExec();
        $this->limitLimit = $start . " , " . $records;
        $this->sql = implode(" ", array($this->sql,$this->limit,$this->limitLimit));
    }

    ############### SELECT STATEMENT END ##################


    ############### INSERT STATEMENT START ##################

    public function insert($table, $values) {

        $params = array_fill(0, count($values), '?');
        //array_unshift($values , 'null');

        $this->sql = implode(" ", array($this->insert, $table, '(', implode(", ",array_keys($values)) ,')', $this->values_start, implode(",",$params), $this->values_end));

        $stmt = $this->db->prepare($this->sql);
        return $stmt->execute(array_values($values));

       //return $this->sql;

    }


    ############### INSERT STATEMENT END ##################

    public function view() {
        $this->whereExec();
        var_dump($this->sql);
    }


     public function fetchAll() {
          $this->whereExec();

          $stmt = $this->db->prepare($this->sql);
          (count($this->whereVals)) ? $stmt->execute($this->whereVals) : $stmt->execute();
          $this->sql = '';
          return $stmt->fetchAll(2);
      }

      public function fetch() {
          $this->whereExec();

          $stmt = $this->db->prepare($this->sql);
          (count($this->whereVals)) ? $stmt->execute($this->whereVals) : $stmt->execute();
          $this->sql = '';
          return $stmt->fetch(2);
      }




}